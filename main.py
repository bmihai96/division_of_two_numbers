#This program has the purpose of computing the division of two numbers
#we will use a function that will have as input two parameters, and the output will the division of those two numbers

def division_of_two_numbers(a,b):
    #We suppose that the user will give the first number 'a' that will be greater or equal to the second number 'b'
    return a/b



if __name__ == '__main__':
    a=float(input("Give first number"))
    b=float(input("Give second number"))
    print("The division of these 2 numbers is :" + str(division_of_two_numbers(a,b)))